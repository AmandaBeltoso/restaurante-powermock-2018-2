package br.ucsal.bes20182.testequalidade.restaurante.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Comanda.class})
public class ComandaUnitarioTest {

	/**
	 * M�todo a ser testado: private Double calcularTotal(). Verificar o c�lculo
	 * do valor total, com um total de 4 itens.
	 * @throws Exception 
	 */
	
	@Test
	public void calcularTotal4Itens() throws Exception {
		
		Mesa mesa = new Mesa(2);
		Item item1 = new Item("item1", 2.00);
		Item item2 = new Item("item2", 5.00);
		Item item3 = new Item("item3", 8.00);
		Item item4 = new Item("item4", 2.00);

		Comanda comanda = new Comanda(mesa);
		Comanda spy = PowerMockito.spy(comanda);
		PowerMockito.when(Comanda.class, "incluirItem()",item1,1);
		PowerMockito.when(Comanda.class, "incluirItem()",item2,1);
		PowerMockito.when(Comanda.class, "incluirItem()",item3,1);
		PowerMockito.when(Comanda.class, "incluirItem()",item4,1);
		Double valorEsperado = 17.0;
		Double valorAtual = Whitebox.invokeMethod(spy, "calcularTotal()");
		Assert.assertEquals(valorEsperado, valorAtual);
	
		
		
		
	}

}
