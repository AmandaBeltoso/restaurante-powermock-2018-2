package br.ucsal.bes20182.testequalidade.restaurante.business;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import br.ucsal.bes20182.testequalidade.restaurante.domain.Mesa;

public class RestauranteBOUnitarioTest {

	/**
	 * M�todo a ser testado: public static Integer abrirComanda(Integer
	 * numeroMesa) throws RegistroNaoEncontrado, MesaOcupadaException. Verificar
	 * se a abertura de uma comanda para uma mesa livre apresenta sucesso.
	 * Lembre-se de verificar a chamada ao ComandaDao.incluir(comanda).
	 * @throws Exception 
	 */
	
	@PrepareForTest({RestauranteBO.class })

	
	public void abrirComandaMesaLivre() throws Exception {
		
		RestauranteBO objeto1 = new RestauranteBO();
		RestauranteBO spy1 = PowerMockito.spy(objeto1);
		PowerMockito.mockStatic(RestauranteBO.class);

		spy1.abrirComanda(2);

	    PowerMockito.verifyStatic();
	    RestauranteBO.abrirComanda(2);
	
		PowerMockito.verifyNoMoreInteractions(RestauranteBO.class);

	}
}
